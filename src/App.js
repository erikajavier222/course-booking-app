import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';

// acquire the pages that will make up the app
import Home from './pages/Home';
import Register from './pages/Register';
import LoginPage from './pages/Login';
import Catalog from './pages/Courses';
import ErrorPage from './pages/Error';
import CourseView from './pages/CourseView';
import AddCourse from './pages/AddCourse';
import UpdateCourse from './pages/UpdateCourse';
import Logout from './pages/Logout';

// acquire provider utility
import { UserProvider } from './UserContext'

// implement page routing in our app
// acquire the utilities from the react router dom
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
// BrowserRouter -> this is standard library component for routing in react. This enables navigation amongst views of various components 
//  as -> alias keyword in vanilla JS

// Routes => it's a new component introduced in V6 of react-router-dom whose task is to allow switching between locations

import './App.css';
// JSX Component -> self closing tag
// syntax: <Element />
function App() {

  // the role of the provider was assigned to the app.js, which means all the information that will be declared here will automatically becomes global.
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // feed the info stated here to the consumers. 
  // Create a side effect that will send a request to API collection to get the identity of the user using the access token that was saved in the browser
  //Create a 'side effect' that will send a request to API collection to get the identity of the user using the access token that was saved in the browser.

  const unsetUser = () => {
    // clear out the saved data 
    localStorage.clear();
    setUser({
      id:null,
      isAdmin: null
    })
  }

  useEffect(() => {
      //identify the state of the user
      // mount or campaign the user to the app so that he/she will be recognized, using the token. 

      //'retrieve' the token from browser storage.
      let token = localStorage.getItem('access');
      
      //fetch('url', 'options'), keep in mind, in this request we are passing down a token.
      fetch('https://warm-forest-76823.herokuapp.com/users/details/', {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }).then(res => res.json()).then(convertedData => {
            // console.log(convertedData);
            // identify the procedures taht will happen if the info  about the user is retrieved successfully
            // change the current state of the user
            if (typeof convertedData._id !== "undefined") {
              setUser({
                id: convertedData._id,
                isAdmin: convertedData.isAdmin
              });
            } else {
              // if the condition above is not met
              setUser({
                id: null,
                isAdmin: null
              });
            }
          });


  },[user]);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar/>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/register' element={<Register />} />
          <Route path="/courses" element={<Catalog />} />
          <Route path='/login' element={<LoginPage/>} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/courses/view/:id' element={<CourseView/>} />
          <Route path='/courses/create' element={<AddCourse />} />
          <Route path='/courses/update' element={<UpdateCourse />} />
          <Route path="*" element={<ErrorPage/>} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;

