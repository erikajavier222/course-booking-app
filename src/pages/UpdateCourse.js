
import Hero from './../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
const data = {
	title: 'Welcome to the Update Course Page',
	content: 'Update Course on this page'
};

export default function Create () {

	const updateCourse = (event) => {
		event.preventDefault()
		return(
			
			Swal.fire(
					{
						icon:"success",
						title:"Successfully Updated course!",
						text: "Thank you for updating course!"

					}
				) 
			);
	};

	return(
		<div>
			<Hero bannerData={data}/>
			<Container>
				<h1 className="text-center">Update Course Form</h1>
				<Form onSubmit={e => updateCourse(e)}>
					{/*Course Name Field*/}
					<Form.Group>
						<Form.Label>Course Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter Updated Course Name" required />
					</Form.Group>

					{/*Description Field*/}
					<Form.Group>
						<Form.Label>Description: </Form.Label>
						<Form.Control type="text" placeholder="Enter Updated Description" required />
					</Form.Group>

					
					{/*Price*/}
					<Form.Group>
						<Form.Label>Price: </Form.Label>
						<Form.Control type="number" placeholder="Enter Updated Price" required />
					</Form.Group>

					{/*Update Course Button*/}
					{/*<Button variant="primary">Enable
					</Button>
					<Button variant="warning">Disable
					</Button>*/}
					
					<Form>
					  <Form.Check 
					    type="switch"
					    id="custom-switch"
					    label="Enable/Disable Course"
					  />
					  
					</Form>
					<Button variant="success" className="btn-block" type="submit">Update
					</Button>
				</Form>
			</Container>
		</div>
		);

}