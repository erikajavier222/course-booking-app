//Identify the components that will be used for this page
/*import Hero from './../components/Banner';*/
import {useContext, useEffect} from 'react'
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

/*const data = {
	title: 'Logout Page',
	content: 'You successfully Logout your account'
}
*/
//create a function that will describe te structure of the page.

export default function Logout() {
	//the important purpose of this module is to 'unomunt' the user from the application
	//Lets destructure our context object

	const {setUser, unsetUser} = useContext(UserContext); 
	//clear out the saved token in the localStorage of the browser
	unsetUser();
	//create a side effect, this effect that we will create will update the state of the global user
	//this is to make sur that the changes about tje user will automatically/ promptly be recognized in a global state
	useEffect(() => {
		//update the global state of the user
		setUser({
			id: null,
			isAdmin: null
		})
	},[setUser])
	return (
		<Navigate to="/login" replace={true} />
		)
}

