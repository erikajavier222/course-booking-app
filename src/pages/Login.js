//Identify the components that will be used for this page

import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Hero from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';
//we will declare stated for our components for us to be able to access and manage the values in each of the form elements.

const data = {
	title: 'Welcome to Login',
	content: 'Sign in you account below'
}

//create a function that will describe te structure of the page.
export default function Login() {
	const {user, setUser} = useContext(UserContext)
	
	//declare an 'initial'/default state for our form elements
	//Bind/Lock the form elements to the desired state
	//Assign the states to their respective components
	//SYNTAX: const/let [getter, setter] = useState()
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');	

	//declare a state for the log in button
	const [isActive, setIsActive] = useState(false);
	const [isValid, setIsValid] = useState(false);

	//create a side effect that will make our page 'reactive'.
	//format: @, dns
	//search a character within a string
	let addressSign = email.search('@');
	//if the search() finds no match inside the string it will return -1.
	let dns = email.search('.com')
	useEffect(() => {
		//Create a logic/condition that will evaluate the format of the email.
		if (dns !== -1 & addressSign !== -1) {
			setIsValid(true);
			if (password !== '') {
				setIsActive(true)
			} else {
				setIsActive(false);
			}
		} else {
			setIsValid(false);
		}
	},[email, password, dns, addressSign])
	

	//apply a cconditional rendering to the bottom component for its current state



	//Create a Simulation that wwill help us idenify the workflow of the login page

	//creatte a function that will run the request for the user authentication to produce an access token
	const loginUser = async (event) => {
		event.preventDefault()

		//send a request to verify if the user's identity if the user is true
		//syntax: fetch ('url', {options})
		fetch('https://warm-forest-76823.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
						email: email,
						password: password
						})
		}).then(res => res.json()).then(data => {
			let token = data.access;
			/*console.log(token);*/

			//create a control structure to give a proper response to the user
			if (typeof token !== 'undefined') {
				//The produced token, save it on the browser storage
				//to save an item in the local storage object of the browser... user setItem(key, value)
									//key, value
				localStorage.setItem('access' ,token)
				fetch('https://warm-forest-76823.herokuapp.com/users/details', {
				 headers: {
				      Authorization: `Bearer ${token}`
				   }
				}).then(res => res.json()).then(convertedData => {
				   /*console.log(convertedData)*/
				   if (typeof convertedData._id !== 'undefined') {
				       setUser({
				          id: convertedData._id,
				          isAdmin: convertedData.isAdmin
				       })
				       /*console.log(user);*/
				       Swal.fire({
				       	icon:"success",
				       	title:"Login Successful!",
				       	text: "TY!"
				       })
				   } else {
				       setUser({
				          id: null,
				          isAdmin: null
				       })
				   }
				}); 

			} else {
				Swal.fire({
					icon:"error",
					title:"Login Error!",
					text: "Check Your Credentials!"
				})
			}
		});

		
	};

	return(
		user.id ?
		<Navigate to="/courses" replace={true}/>

		:

		<>
			<Hero bannerData={data}/>
			<Container>
				<h1 className="text-center">Login Form</h1>
				<Form onSubmit={e => loginUser(e)}>
					{/*Email Address*/}
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" placeholder="Enter Email Here" required value={email} onChange={event => {setEmail(event.target.value)} }/>
						{
							isValid ? 
								<h6 className="text-success">Email is Valid!</h6>
							:
								<h6 className="text-mute">Email is Invalid!</h6>
						}
					</Form.Group>

					{/*Password*/}
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter Password Here" required value={password} onChange={event => setPassword(event.target.value)} />
					</Form.Group>
					{
						isActive ?
							<Button type="submit" variant="success" className="btn-block">Login</Button>
						:
							<Button type="submit" variant="secondary" className="btn-block" disabled>Login</Button>
					}
				</Form>
			</Container>
		</>
		);
}